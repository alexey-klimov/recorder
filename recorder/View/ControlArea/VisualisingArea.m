//
//  VisualisingArea.m
//  recorder
//
//  Created by Alexey Klimov on 26.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <math.h>
#import <stdlib.h>

#import "VisualisingArea.h"

@interface PCMAudio ()
@property (nonatomic) NSMutableArray * mSamples;
@end

@implementation PCMAudio

- (instancetype) init
{
	if (self = [super init])
    {
        self.samplesPerSecond = 120;
        self.mSamples = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) addAmplitude:(double)amplitude forTime:(NSTimeInterval)seconds
{
    NSTimeInterval valueForIndex = seconds * self.samplesPerSecond;
	assert( valueForIndex < NSUIntegerMax );
    NSUInteger index = (NSUInteger)valueForIndex;
	
    if (index >= self.mSamples.count)
    {
    	static const double defaultValue = 0.0;
        NSUInteger dummySamplesCountToAdd = (index + 1) - self.mSamples.count;
        for (NSUInteger i = 0; i < dummySamplesCountToAdd; ++i)
            [self.mSamples addObject:@(defaultValue)];
    }
    
    amplitude = fabs(amplitude);
    self.mSamples[index] = @(amplitude);
}

- (double) amplitudeForTime:(NSTimeInterval)seconds
{
    NSTimeInterval valueForIndex = seconds * self.samplesPerSecond;
	assert( valueForIndex <= NSUIntegerMax );
    NSUInteger index = (NSUInteger)valueForIndex;
    if (index >= self.mSamples.count)
    	return 0.0;
    
    return [self.mSamples[index] doubleValue];
}

- (void) clear
{
    [self.mSamples removeAllObjects];
}

@end

/*====================================================================*/

const CGFloat kLineWidth = 1;
const CGFloat kPixelPerfectOffset = kLineWidth / 2;

static inline CGFloat pixelPerfect(CGFloat v)
{
	return v + kPixelPerfectOffset;
}

typedef struct CGPoint Offset;
static inline void
pixelPerfectLine( CGContextRef c, CGPoint origin, Offset offset )
{
    assert(!( (bool)offset.x && (bool)offset.y
        || !(bool)offset.x && !(bool)offset.y)
    );

    if (!(bool)offset.x)
        origin.x = pixelPerfect(origin.x);
    else
        origin.y = pixelPerfect(origin.y);

    CGContextMoveToPoint(c, origin.x, origin.y);
    CGContextAddLineToPoint(c, origin.x + offset.x, origin.y + offset.y);
}

/*====================================================================*/

@interface VisualisingArea ()

@property (nonatomic) CGFloat   kSecondUnitWidth;
@property (nonatomic) CGFloat   kSecondDelimiterHeight;

@property (nonatomic) NSInteger kSubunitsCountInSecondUnit;
@property (nonatomic) CGFloat   kSubunitWidth;
@property (nonatomic) CGFloat   kSubunitDelimiterHeight;

@property (nonatomic) CGFloat   kSecondLabelHeight;
@property (nonatomic) UIFont*   kSecondLabelFont;
@property (nonatomic) CGFloat   kSecondLabelOffsetY; // Get rid of ascender extra space


@property (nonatomic) NSArray*  kAmplitudeAxisDelimiters;
@property (nonatomic) CGFloat   kAmplitudeLabelHeight;
@property (nonatomic) UIFont*   kAmplitudeLabelFont;

@property (nonatomic) CGFloat   kCursorEndRadius;
@property (nonatomic) CGFloat	mCursorPositionX;
@property (nonatomic) CGFloat   kZeroSecondPositionX;

@property (nonatomic) CGFloat   kWaveformAreaTopY;
@property (nonatomic) CGFloat   kWaveformAreaHeight;

@property (nonatomic) PCMAudio * amplitudeByTime;

@end

/*====================================================================*/

@implementation VisualisingArea

/*====================================================================*/

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.kZeroSecondPositionX = 0 + self.bounds.size.width / 2;
}

-(void)setCursorTimePosition:(NSTimeInterval) seconds
{
    _cursorTimePosition = seconds;
    self.contentOffset =
        CGPointMake((float)seconds * self.kSecondUnitWidth,0);
    
    {
        CGSize contentSize = self.contentSize;
        CGFloat preferredWidth = self.mCursorPositionX + 2 * self.bounds.size.width;
        contentSize.width = (contentSize.width < preferredWidth)
        ? preferredWidth : contentSize.width;
        self.contentSize = contentSize;
    }
}

- (void) setContentOffset:(CGPoint)contentOffset
{
    [super setContentOffset:contentOffset];
    self.mCursorPositionX = self.bounds.origin.x + self.bounds.size.width / 2;
}

/*====================================================================*/

-(void) setUpTimeAxisConstants
{
    self.kSecondUnitWidth = 45;
    self.kSecondDelimiterHeight = 16;

    self.kSubunitsCountInSecondUnit = 4;
    self.kSubunitWidth = self.kSecondUnitWidth / self.kSubunitsCountInSecondUnit;
    self.kSubunitDelimiterHeight = self.kSecondDelimiterHeight / 8;

    self.kSecondLabelHeight = (self.kSecondDelimiterHeight - self.kSubunitDelimiterHeight)*3/5;
    self.kSecondLabelFont = [self determineFontForHeight:self.kSecondLabelHeight];
    self.kSecondLabelOffsetY = self.kSecondLabelFont.ascender-self.kSecondLabelFont.capHeight;
}

-(void) setUpCursor
{
    self.kCursorEndRadius = 2;
    [self setContentOffset:CGPointMake(0,0)];
}

-(void) setUpWaveform // after cursor setup
{
    self.kCursorEndRadius = 2;
    self.kWaveformAreaTopY = self.kSecondLabelOffsetY + self.kSecondDelimiterHeight +
    	kLineWidth;
    self.kWaveformAreaHeight = kVisualizingAreaHeight - 2 * self.kCursorEndRadius -
            self.kWaveformAreaTopY - kLineWidth;

    
    self.amplitudeByTime = [[PCMAudio alloc] init];
    self.amplitudeByTime.samplesPerSecond =
        (NSInteger)(self.kSecondUnitWidth * [UIScreen mainScreen].scale);
}

- (void) setUpDummyPCM
{
    for (int i = 0; i < 40; ++i)
    {
        for (int j = 0; j < self.amplitudeByTime.samplesPerSecond; ++j)
        {
            [self.amplitudeByTime addAmplitude:rand() % 65500
               forTime:i + (double)j/self.amplitudeByTime.samplesPerSecond
            ];
        }
    }
}

-(void) setUpAmplitudeAxisConstants // After waveform setup
{
    self.kAmplitudeAxisDelimiters = @[@0, @-1, @-2, @-3, @-5, @-7, @-10];

    self.kAmplitudeLabelHeight = (self.kWaveformAreaHeight / 2 - kLineWidth) /
            self.kAmplitudeAxisDelimiters.count;
    self.kAmplitudeLabelFont = [self determineFontForHeight:self.kSecondLabelHeight - 3];
}

-(void) setUp
{
	[self setUpTimeAxisConstants];
    
    [self setUpCursor];
    [self setUpWaveform];
    
    [self setUpAmplitudeAxisConstants];

    self.showsHorizontalScrollIndicator = NO;
}

- (UIFont*) determineFontForHeight: (CGFloat)height
{
	CGFloat fontHeight = 0;
    CGFloat fontSize = 1;
	while (height > fontHeight)
    {
        fontSize++;
        fontHeight = [UIFont systemFontOfSize:fontSize].capHeight;
    }
    return [UIFont systemFontOfSize:fontSize-1];
}


- (void)setDefaultColor:(CGContextRef)c
{
	CGContextSetRGBStrokeColor(c, 1, 1, 1, 0.5);
}

/*====================================================================*/

- (void)drawRect:(CGRect)rect
{
    CGContextRef c = UIGraphicsGetCurrentContext();
	[self setDefaultColor: c];
    CGContextSetLineWidth(c, kLineWidth);

	[self drawCursor:c rect:rect];
    
    [self drawAmplitudeValues:c rect:rect];
    [self drawAmplitudeAxis:c rect:rect];
    
    [self drawTimeAxisLines:c rect:rect];
    [self drawTimeDelimiters:c rect:rect];
}

/*====================================================================*/

- (void)drawTimeAxisLines: (CGContextRef)c rect:(CGRect)rect
{
    [self setDefaultColor: c];
    // top
    {
    	CGContextBeginPath(c);

		CGPoint p = {rect.origin.x, rect.origin.y + self.kWaveformAreaTopY -
        	kLineWidth};
    	Offset o = {rect.size.width,0};
        pixelPerfectLine(c, p, o);
        
        CGContextStrokePath(c);
    }
    
    // middle
    {
    	CGContextBeginPath(c);
    	CGContextSetRGBStrokeColor(c, 1, 1, 1, 0.6f);
    	
        CGPoint p = {rect.origin.x, rect.origin.y + self.kWaveformAreaTopY +
                self.kWaveformAreaHeight / 2};
    	Offset o = {rect.size.width,0};
    	pixelPerfectLine(c, p, o);
    	
    	CGContextStrokePath(c);
        [self setDefaultColor: c];
    }
    
    // bottom
    {
    	CGContextBeginPath(c);
        
        CGPoint p = {rect.origin.x,
        	rect.origin.y + self.kWaveformAreaTopY + self.kWaveformAreaHeight};
    	Offset o = {rect.size.width,0};
    	pixelPerfectLine(c, p, o);
        
        CGContextStrokePath(c);
    }
}

- (void)drawLabelForSecond:(NSInteger)second inContext:(CGContextRef)c atPoint:(CGPoint)origin
{
	NSInteger minute = second / 60;
    second %= 60;
    NSString * string =
    	[NSString stringWithFormat:@"%.2ld:%.2ld",
        	(unsigned long)minute, (unsigned long)second
        ];
    
    CGPoint realOrigin = origin;
    realOrigin.y -= self.kSecondLabelOffsetY;

    [string drawAtPoint: realOrigin
    	withAttributes:@{
        	NSFontAttributeName: self.kSecondLabelFont,
            NSForegroundColorAttributeName: [UIColor whiteColor]
    }];
}

- (void)drawTimeDelimiters: (CGContextRef)c rect:(CGRect)rect
{
    CGContextSetRGBStrokeColor(c, 1, 1, 1, 0.25);

    static const NSInteger kOutOfBoundsDelimitersCount = 8;
    
    CGFloat firstSecondDelimiterPositionX =
        (float)fmod(self.kZeroSecondPositionX, self.kSecondUnitWidth) -
            kOutOfBoundsDelimitersCount * self.kSecondUnitWidth;

    NSInteger kDelimitersCount =
        (NSInteger)(self.contentSize.width / self.kSecondUnitWidth) +
            2 * kOutOfBoundsDelimitersCount;
    
    for (int i = 0; i < kDelimitersCount; ++i)
    {
        // Unit delimiter
        {
            CGContextBeginPath(c);
            CGPoint p = {firstSecondDelimiterPositionX + i * self.kSecondUnitWidth,
                rect.origin.y + self.kWaveformAreaTopY - kLineWidth};
            Offset o = {0, -self.kSecondDelimiterHeight};
            pixelPerfectLine(c, p, o);
            CGContextStrokePath(c);
        }

        // Subunit delimiters
        CGContextSetRGBStrokeColor(c, 1, 1, 1, 0.25);
        for (int j = 1; j < self.kSubunitsCountInSecondUnit; ++j)
        {
            CGContextBeginPath(c);
            CGPoint p = {firstSecondDelimiterPositionX + i*self.kSecondUnitWidth +
            	j*self.kSubunitWidth, self.kWaveformAreaTopY - kLineWidth};
            Offset o = {0,-self.kSubunitDelimiterHeight};
            pixelPerfectLine(c, p, o);
            CGContextStrokePath(c);
        }
    }

    // Text labels for delimiters
    CGFloat startX = self.kZeroSecondPositionX;
    NSInteger kSecondsCount =
    	(NSInteger)((self.contentSize.width - startX) / self.kSecondUnitWidth) +
            kOutOfBoundsDelimitersCount;
    for (int i = 0; i < kSecondsCount; ++i)
    {
        CGPoint textLabelPoint = {
            startX + i * self.kSecondUnitWidth +
            	kLineWidth + self.kSecondUnitWidth / 12,
            rect.origin.y + self.kWaveformAreaTopY -
            	kLineWidth -self.kSecondDelimiterHeight
        };
        [self drawLabelForSecond:i inContext:c atPoint:textLabelPoint];
    }
    [self setDefaultColor:c];
}

/*====================================================================*/

- (void)drawAmplitudeAxisGradient: (CGContextRef)c rect:(CGRect)rect
{
    // Draw gradient behind the delimiters
    CGGradientRef gradient;
    CGColorSpaceRef colorspace;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 0, 0, 0, 0,  // Start color
                              0, 0, 0, 1 }; // End color

    colorspace = CGColorSpaceCreateWithName(kCGColorSpaceSRGB);
    gradient = CGGradientCreateWithColorComponents(colorspace, components, locations, 2);
	
    CGPoint startPoint, endPoint;
    startPoint.x = rect.origin.x;
    startPoint.y = rect.origin.y;
    endPoint.x = rect.origin.x + rect.size.width;
    endPoint.y = startPoint.y;
    
    CGContextSaveGState(c);
    
    CGContextClipToRect(c, rect);
    CGContextDrawLinearGradient (c, gradient, startPoint,
    	endPoint, kCGGradientDrawsBeforeStartLocation
    );
    CGContextRestoreGState(c);
    
    CGColorSpaceRelease(colorspace);
    CGGradientRelease(gradient);

}

- (void)drawAmplitudeAxis: (CGContextRef)c rect:(CGRect)rect
{
	CGFloat maxDelimiterWidth = 24;
    CGRect gradientRect = {
    	{rect.origin.x + rect.size.width - maxDelimiterWidth, self.kWaveformAreaTopY},
        {maxDelimiterWidth, self.kWaveformAreaHeight}
    };
    [self drawAmplitudeAxisGradient:c rect:gradientRect];
    
    // Draw delimiters
    NSMutableParagraphStyle *paragraphStyle =
        [[NSMutableParagraphStyle alloc] init];
	paragraphStyle.alignment = NSTextAlignmentRight;

    for (NSUInteger i = 0; i < self.kAmplitudeAxisDelimiters.count; ++i)
    {
    	NSNumber * value = self.kAmplitudeAxisDelimiters[i];
        NSString * string =
            [NSString stringWithFormat:@"%ld", (long)value.integerValue];

        NSAttributedString * attributedString =
            [[NSAttributedString alloc] initWithString:string attributes: @{
            	NSFontAttributeName: self.kAmplitudeLabelFont,
                NSForegroundColorAttributeName: [UIColor whiteColor]
            }];
        
        CGSize size = [attributedString size];

        CGPoint above = {
            rect.origin.x + rect.size.width - size.width,
                self.kWaveformAreaTopY + i * self.kAmplitudeLabelHeight
        };
        CGPoint below = {
            above.x,
                self.kWaveformAreaTopY + self.kWaveformAreaHeight -
                        (i+1) * self.kAmplitudeLabelHeight
        };
        
        [attributedString drawAtPoint:above];
        [attributedString drawAtPoint:below];
    }
}

- (void)drawAmplitudeValues: (CGContextRef)c rect:(CGRect)rect
{
    CGContextSaveGState(c);
    
    CGContextClipToRect(c,
        CGRectMake(
           rect.origin.x, self.kWaveformAreaTopY,
           rect.size.width, self.kWaveformAreaHeight
    ));
    
    CGFloat firstSecondX = self.kZeroSecondPositionX;
    CGFloat startX = (firstSecondX < rect.origin.x) ? rect.origin.x : firstSecondX;
    
    for (int i = (int)startX; i<rect.size.width + rect.origin.x; i++)
    {
    	if ( !(i % 3) )
        	continue;
        CGFloat value = (CGFloat)
        [self.amplitudeByTime
        	amplitudeForTime:((CGFloat)i - firstSecondX)/self.kSecondUnitWidth
        ];
        
        if (value == 0.0)
            continue;
        
        // TODO: Get rid of magic number
        CGFloat normalizedValue = value / (15536 / (self.kWaveformAreaHeight / 2));
    
        normalizedValue = (CGFloat)fabs(normalizedValue);
        static const double minAmiplitude = 2.0;
        if (normalizedValue < minAmiplitude)
            normalizedValue = (CGFloat)(minAmiplitude + normalizedValue);
        CGContextBeginPath(c);
    
    	CGPoint p = { i, self.kWaveformAreaTopY + kPixelPerfectOffset +
                self.kWaveformAreaHeight / 2 - normalizedValue };
    	Offset o = {0, 2 * normalizedValue};
    	pixelPerfectLine(c, p, o);
    	CGContextStrokePath(c);
    }
    
    CGContextRestoreGState(c);
}

/*====================================================================*/


- (void)drawCursor: (CGContextRef)c rect:(CGRect)rect
{
	CGContextSetRGBStrokeColor(c, 0, 0, 1, 1);
    CGContextSetRGBFillColor(c, 0, 0, 1, 1);
    
    //top circle
    {
    	CGContextBeginPath(c);
    
    	CGContextAddArc(c, pixelPerfect(rect.origin.x + rect.size.width / 2),
                self.kWaveformAreaTopY - kLineWidth - self.kCursorEndRadius,
                self.kCursorEndRadius, 0, 2 * (CGFloat)M_PI, 1
    	);
    
    	CGContextFillPath(c);
    	CGContextStrokePath(c);
    }
    
    //middle line
    {
    	CGContextBeginPath(c);
    	CGPoint p = {
	    	rect.origin.x + rect.size.width / 2,
    		rect.origin.y + self.kWaveformAreaTopY
    	};
    	Offset o = {0, self.kWaveformAreaHeight + kLineWidth};
    	pixelPerfectLine(c, p, o);
    	CGContextStrokePath(c);
    }
    
    //bottom circle
    {
    	CGContextBeginPath(c);
    	CGContextAddArc(c, pixelPerfect(rect.origin.x + rect.size.width / 2),
    		rect.origin.y + self.kWaveformAreaTopY + self.kWaveformAreaHeight +
             	kLineWidth + self.kCursorEndRadius,
                self.kCursorEndRadius, 0, 2 * (CGFloat)M_PI, 1
    	);
        CGContextFillPath(c);
    	CGContextStrokePath(c);
    }
    
    [self setDefaultColor:c];
}

@end
