//
//  VisualisingArea.h
//  recorder
//
//  Created by Alexey Klimov on 26.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kVisualizingAreaHeight = 150;


@interface PCMAudio : NSObject

@property (nonatomic) NSInteger samplesPerSecond;

- (void) addAmplitude:(double)amplitude forTime: (NSTimeInterval)seconds;
- (double) amplitudeForTime: (NSTimeInterval)seconds;

- (void) clear;

@end


@interface VisualisingArea : UIScrollView

@property (readonly, nonatomic) PCMAudio * amplitudeByTime;
@property (nonatomic) NSTimeInterval cursorTimePosition;

- (void) setUp;

@end
