//
//  Record.h
//  recorder
//
//  Created by Alexey Klimov on 17.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Record : NSObject

@property (nonatomic, readonly, nonnull) NSString * name;
@property (nonatomic, readonly) NSTimeInterval recordedLength;
@property (nonatomic, readonly, nonnull) NSDate* creationDate;

- (instancetype _Nullable) initWithURL:(NSURL* _Nonnull)fileURL;
@property (nonatomic, readonly, nonnull) NSURL * fileURL;

@end

/*

### record from given time

## approach #1
- Write to another file with standard classes.
- Merge into original file with using direct binary representation.

## approach #2

- Write to buffer with standard classes ??? How?
- Merge into original file with using direct binary representation.
*/

// *** Interfaces for view controller: ***
// [recordController newRecord]
// [recordController getRecordsCount]
// [recordController getRecordAtRow]
// [recordController removeRecordAtRow]


// record get name
// record set new name

// record get creation date
// record get recorded time length
// record start recording from beginning
// pause recording
// record stop recording

// record play recorded from given time using specified headphone - out of box
// record stop playing recorded - out of box

// record subscribe for current time position change
// record get amplitude/time points for time range


// *** Storing content on disk: ***

// Record name = file name
// Record writes/reads file
// Record asks RecordController if destination file exists (for "record set new name")
// Record handles no space error
// Record handles no memory error ??? only when writing to buffer first.

// RecordController finds unique name for new record.
// RecordController finds out if destination file exists ->
// -> (for "record set new name" and "recordController newRecord")