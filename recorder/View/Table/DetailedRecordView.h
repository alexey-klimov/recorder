//
//  DetailedRecordView.h
//  recorder
//
//  Created by Alexey Klimov on 03.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "RecordCell.h"

@class TwoImageSwitchingButton;

@interface DetailedRecordView : UIView

- (void) initSeparatorWithColor: (UIColor*)_color;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *time;

@property (weak, nonatomic) IBOutlet UILabel  * playerTime;
@property (weak, nonatomic) IBOutlet UISlider * progressBar;
@property (weak, nonatomic) IBOutlet UILabel  * playerTimeLeft;
@property (weak, nonatomic) IBOutlet TwoImageSwitchingButton * playOrPauseButton;

@property (weak, nonatomic) IBOutlet UIButton * shareButton;
@property (weak, nonatomic) IBOutlet UIButton * removeButton;

@end
