//
//  RecordTableView.m
//  recorder
//
//  Created by Alexey Klimov on 16.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "RecordTableView.h"
#import "RecordCell.h"
#import "InactiveLayerView.h"
#import "DetailedRecordView.h"
#import "RecordTableController.h"


@interface RecordTableView () <UITableViewDelegate>

@property (nonatomic) IBOutlet UITableView *mTableView;

@property (nonatomic) InactiveLayerView * mTopInactivityLayer;
@property (nonatomic) InactiveLayerView * mBottomInactivityLayer;

@property (nonatomic) RecordCell* mSelectedTableCell;
@property (nonatomic) NSInteger selectedRow;

@property (nonatomic) DetailedRecordView *mDetailed;
@property (nonatomic) NSArray<NSLayoutConstraint *> * mDetailedPositionConstraints;

@property (nonatomic) BOOL mIsUpdating;

@end

@implementation RecordTableView

- (void) reset
{
    [UIView performWithoutAnimation:^{
        [self deselectRow];
        [self.mTopInactivityLayer reset];
        [self.mBottomInactivityLayer reset];
    }];
}

- (void) setController:(RecordTableController*)controller
{
	_controller = controller;
    self.mTableView.dataSource = controller;
}

- (void) setEditing:(BOOL)value
{
    [self.mTableView setEditing:value animated:YES];
}

- (void)setAnimationDuration:(NSTimeInterval)animationDuration
{
    _animationDuration = animationDuration;
    self.mTopInactivityLayer.animationDuration = animationDuration;
    self.mBottomInactivityLayer.animationDuration = animationDuration;

}

- (RecordCell*)cellForRow: (NSInteger)row
{
	NSIndexPath * path = [NSIndexPath indexPathForRow:row inSection:0];
    return [self.mTableView cellForRowAtIndexPath:path];
}

- (void) setUpWithDetailedRecordView:(DetailedRecordView*)_detailedView
{
    self.mDetailed = _detailedView;
    
    self.enabled = YES;
    [self initTable];
    [self initDetailedRecordView];
    [self initInactivityLayers];
    
	[self resetSelectedCellInfo];
}

- (void) resetSelectedCellInfo
{
    self.mSelectedTableCell = nil;
    self.selectedRow = -1;
}

- (void) reloadData
{
    // Prevent simultaneous reloading of table
    // For more explanation look in RecordTableController.m: cellForRowAtIndexPath
    
    if (self.mIsUpdating)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadData];
        });
        return;
    }

    self.mIsUpdating = YES;
    [UIView animateWithDuration:0 animations:^{
        [self.mTableView reloadData];
    } completion:^(BOOL finished){ if (finished) { self.mIsUpdating = NO; } }];
}

- (void) deleteRow:(NSInteger)row
{
    [self deselectRowAndAnimateDeleting:(row == self.selectedRow)];

	[UIView performWithoutAnimation:^{
    [self.mTableView
    	deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]]
        withRowAnimation:UITableViewRowAnimationNone
    ];
    }];
}

- (void)initTable {
    RecordCell* prototype =
    	[self.mTableView dequeueReusableCellWithIdentifier:[RecordCell identifier]];
    [prototype setNeedsLayout];
    [prototype layoutIfNeeded];
    
    self.mTableView.estimatedRowHeight = prototype.bounds.size.height;
    self.mTableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)initDetailedRecordView {
	
    self.mDetailed.hidden = YES;
    [self addSubview:self.mDetailed];
    
    [self.mDetailed initSeparatorWithColor:self.mTableView.separatorColor];
    
    self.mDetailedPositionConstraints = nil;
}

- (void)initInactivityLayers {
    CGRect hidden = CGRectMake(0, 0, 0, 0);
    self.mTopInactivityLayer = [[InactiveLayerView alloc] initWithFrame:hidden];
    self.mBottomInactivityLayer = [[InactiveLayerView alloc] initWithFrame:hidden];
	
    [[self.mTopInactivityLayer.gestureRecognizers objectAtIndex:0]
    	addTarget:self action:@selector(deselectRow)];
    [[self.mBottomInactivityLayer.gestureRecognizers objectAtIndex:0]
    	addTarget:self action:@selector(deselectRow)];
    
    self.mTopInactivityLayer.hidden = YES;
    self.mBottomInactivityLayer.hidden = YES;
    
    [self addSubview:self.mTopInactivityLayer];
    [self addSubview:self.mBottomInactivityLayer];
}

- (void) setEnabled: (BOOL) value
{
	_enabled = value;
    
    self.userInteractionEnabled = value;
    
    self.mTopInactivityLayer.frame = self.bounds;
    if (value)
    	[self.mTopInactivityLayer reset];
    else
    	[self.mTopInactivityLayer becomeInactive];
    self.mTopInactivityLayer.hidden = value;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)path
{
    // Ingore selecting during bouncing at top
    if (tableView.contentOffset.y < 0)
    	return NO;
    
    // Ingore selecting during bouncing at bottom
    if ((tableView.contentOffset.y != 0)
    && ((tableView.contentSize.height - tableView.contentOffset.y) < tableView.bounds.size.height))
    	return NO;

	[self showDetailsForRecord:path];
    return NO;
}

- (void)showInactivityLayerAboveRect: (CGRect) _cellRect
{
    CGRect topLayerRect = CGRectMake(0, 0, _cellRect.size.width, _cellRect.origin.y);
    
    self.mTopInactivityLayer.frame = topLayerRect;
    [self.mTopInactivityLayer captureUnderneathContentImage];
    [self.mTopInactivityLayer becomeInactive];
    self.mTopInactivityLayer.hidden = NO;
}

- (void)showInactivityLayerBelowRect:(CGRect)_cellRect
{
	CGFloat bottomY = _cellRect.origin.y + _cellRect.size.height;
    CGRect bottomLayerRect = CGRectMake(0,
    	bottomY, _cellRect.size.width,
        self.bounds.size.height - bottomY
    );
    
    self.mBottomInactivityLayer.frame = bottomLayerRect;
    [self.mBottomInactivityLayer captureUnderneathContentImage];
    [self.mBottomInactivityLayer becomeInactive];
    self.mBottomInactivityLayer.hidden = NO;
}

- (void)showDetailedRecordViewAtopCell:(RecordCell*)_cell
{
    // TODO: Get rid of code duplication:
    CGRect cellRect = _cell.frame;
    cellRect.origin.y -= self.mTableView.contentOffset.y;
	
    // TODO: support layout changes (e.g. autorotation)
    self.mDetailed.layoutMargins = _cell.layoutMargins;
	
    self.mDetailedPositionConstraints = @[
    	[self.mDetailed.topAnchor constraintEqualToAnchor:self.topAnchor constant:cellRect.origin.y],
    	[self.mDetailed.widthAnchor constraintEqualToConstant:cellRect.size.width],
        [self.mDetailed.leadingAnchor constraintEqualToAnchor:self.leadingAnchor],
    ];
    [NSLayoutConstraint activateConstraints:self.mDetailedPositionConstraints];
    
    [self.mDetailed layoutIfNeeded];
    
    self.mDetailed.hidden = NO;
}

- (void)expandDetailedView{
	// 1. Cell is at top and partially visible => scroll to top and use 5
    // 2. Cell is at top and fully visible => use 5
    // 3. Cell is at bottom and partly visible => scroll to bottom and use 4
    // 4. Cell is at bottom and fully visible but won't be fully visible after expanding =>
    //	  move detailed and top inactivity layer up
    // 5. Cell is visible and will be fully visible after expanding =>
    //    move bottom inactivity layer down
    
    CGFloat detailedBottomY = self.mDetailed.frame.origin.y + self.mDetailed.frame.size.height;
    CGFloat tableBottomY = self.bounds.size.height;
    CGFloat detailedBottomYOffset = detailedBottomY - tableBottomY;
    if (detailedBottomYOffset > 0)
    {
        CGRect topLayerRect = self.mTopInactivityLayer.frame;
    	topLayerRect.origin.y -= detailedBottomYOffset;
        CGRect bottomLayerRect = self.mBottomInactivityLayer.frame;
    	bottomLayerRect.origin.y = self.mDetailed.frame.origin.y + self.mDetailed.frame.size.height;
        
        [self.mDetailedPositionConstraints objectAtIndex:0].constant -= detailedBottomYOffset;
        [UIView animateWithDuration:self.animationDuration animations:^{
    		self.mTopInactivityLayer.frame = topLayerRect;
            self.mBottomInactivityLayer.frame = bottomLayerRect;
            [self layoutIfNeeded];
    	}];
    }
    else
    {
        CGRect bottomLayerRect = self.mBottomInactivityLayer.frame;
    	bottomLayerRect.origin.y = self.mDetailed.frame.origin.y + self.mDetailed.frame.size.height;
        [UIView animateWithDuration:self.animationDuration animations:^{
    		self.mBottomInactivityLayer.frame = bottomLayerRect;
    	}];
    }
}

- (void)makeCellFullyVisible: (NSIndexPath *)path
{
	RecordCell * cell = [self.mTableView cellForRowAtIndexPath: path];
    [self.mTableView scrollRectToVisible:cell.frame animated:NO];
}

- (void)showDetailsForRecord: (NSIndexPath *)path
{
    [self.controller recordWillBecomeSelected:path.row];
    
    [UIView animateWithDuration:self.animationDuration
    	animations:^{ [self makeCellFullyVisible:path]; }
        completion:^(BOOL done){ if(!done) return;
        	self.mSelectedTableCell = [self.mTableView cellForRowAtIndexPath: path];
            self.selectedRow = path.row;
        	CGRect cellRect = self.mSelectedTableCell.frame;
    		cellRect.origin.y -= self.mTableView.contentOffset.y;
    
    		[self showInactivityLayerAboveRect:cellRect];
    		[self showInactivityLayerBelowRect:cellRect];
    		[self showDetailedRecordViewAtopCell:self.mSelectedTableCell];
    
    		[self expandDetailedView];
        }
    ];
}

#pragma mark - Detailed record view hiding

-(void)collapseDetailedRecordViewToRecordCell: (RecordCell*)_cell
	animateDeleting: (BOOL)deleteCell
{
	CGFloat cellTopY = _cell.frame.origin.y - self.mTableView.contentOffset.y;
    
    CGRect topLayerRect = self.mTopInactivityLayer.frame;
    topLayerRect.origin.y = cellTopY - topLayerRect.size.height;
    self.mTopInactivityLayer.frame = topLayerRect;
    
    CGRect bottomLayerRect = self.mBottomInactivityLayer.frame;
    bottomLayerRect.origin.y = cellTopY +
    	(deleteCell ? 0 : _cell.frame.size.height);
    self.mBottomInactivityLayer.frame = bottomLayerRect;
    
    [self.mDetailedPositionConstraints objectAtIndex:0].constant = cellTopY;
    [self layoutIfNeeded];
}

- (void)deselectRow
{
	if (self.selectedRow != -1)
        [self deselectRowAndAnimateDeleting:NO];
}

-(void)deselectRowAndAnimateDeleting: (BOOL)animateDeleting
{
    [self.controller recordWillBecomeDeselected:self.selectedRow];
    
    [UIView animateWithDuration:self.animationDuration animations:^{
    	[self collapseDetailedRecordViewToRecordCell:self.mSelectedTableCell
        	animateDeleting:animateDeleting
        ];
        [self.mTopInactivityLayer becomeActive];
        [self.mBottomInactivityLayer becomeActive];
    } completion: ^(BOOL done){ if (!done) return;
    	[self.mTopInactivityLayer reset];
    	[self.mBottomInactivityLayer reset];
    	self.mDetailed.hidden = YES;
    
    	[NSLayoutConstraint deactivateConstraints:self.mDetailedPositionConstraints];
    	self.mDetailedPositionConstraints = nil;
    	self.mSelectedTableCell = nil;
        
        [self resetSelectedCellInfo];
    }];
}

@end
