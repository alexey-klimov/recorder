//
// Created by Alexey Klimov on 19.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

#import <Foundation/Foundation.h>

// Can be used in arbitrary thread

@interface ErrorHandler : NSObject

+ (instancetype) sharedInstance;
- (BOOL) handleIfNeeded: (NSError*)error;
- (BOOL) handleUnrecoverableIfNeeded: (NSError*)error;
- (void) handle: (NSString*)message;
- (void) handleUnrecoverable: (NSString*)message;

@end