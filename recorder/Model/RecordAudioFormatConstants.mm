#import "RecordAudioFormatConstants.h"

NSNumber * kRCOutputAudioStreamFormat = @(kAudioFormatMPEG4AAC);
NSNumber * KRCOutputAudioSampleRate = @(44100);
NSNumber * KRCOutputAudioNumberOfChannels = @(1);
NSString * kRCAudioFileFormat = AVFileTypeAppleM4A;
NSString * kRCAudioFileFormatExtension = @"m4a";
