//
// Created by Alexey Klimov on 18.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerRecorderBase.h"

@class Record;

/*============================================================================*/

// TODO: Protocols should be merged into one and rewritten using one-many event sending

@protocol RecorderObserver

- (void) recordedDurationDidChange: (NSInteger)value;

@end

@protocol RecordedAudioStreamObserver <NSObject>

- (NSInteger) expectedObservingRate;
- (void) handleAmplitude: (double)amplitude forTime: (NSTimeInterval)timeInSeconds;
- (void) recordingWasInterrupted;

@end

/*============================================================================*/

// Called only from Main dispatch queue

@interface Recorder : PlayerRecorderBase

@property (nonatomic) id<RecorderObserver> recorderObserver;
@property (nonatomic) id<RecordedAudioStreamObserver> recordedAudioStreamObserver;

- (void) startRecording: (Record*)record;
- (void) pauseRecording;
- (void) stopRecording;

@end

/*============================================================================*/