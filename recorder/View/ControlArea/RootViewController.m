//
//  RootViewController.m
//  recorder
//
//  Created by Alexey Klimov on 25.05.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "RootViewController.h"

#import "TwoImageSwitchingButton.h"
#import "RecordButton.h"
#import "TitleSwitchButton.h"

#import "VisualisingArea.h"

@interface RootViewController () <RecordedAudioStreamObserver>

@property (nonatomic) BOOL mRecordingModeEnabled;
@property (nonatomic) RecordTableController * mTableController;


@property (weak, nonatomic) IBOutlet RecordButton *mRecordButton;
@property (weak, nonatomic) IBOutlet TwoImageSwitchingButton *mPlayButton;
@property (weak, nonatomic) IBOutlet TitleSwitchButton *mEditButton;
@property (weak, nonatomic) IBOutlet UIButton *mDoneButton;

@property (nonatomic) NSTimeInterval mAnimationDuration;
@property (weak, nonatomic) IBOutlet VisualisingArea *mVisualizingArea;

@end

@implementation RootViewController

- (UIStatusBarStyle) preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLoad
{
    [super viewDidLoad];

    self.mAnimationDuration = 0.2f;

    [self setUpTableView];
    [self setUpRecordButton];
    [self setUpPlayButton];
    [self setUpEditButton];
    [self setUpSpeakerButton];
    [self setUpDoneButton];

    [self setRecordingModeTo:NO];
    [self.mVisualizingArea setUp];
}

- (void) setRecordController: (RecordController*)rc
{
    _recordController = rc;

    self.recordController.recorder.recordedAudioStreamObserver = self;
    self.mTableController.recordController = self.recordController;
}

/*====================================================================*/

- (void) setUpTableView
{
	self.mTableController = self.childViewControllers[0];
	[self.mTableController setAnimationDuration:self.mAnimationDuration];
}

- (void) setUpRecordButton
{
    self.mRecordButton.animationDuration = self.mAnimationDuration;
    [self.mRecordButton subscribeTarget:self withAction:@selector(recordButtonPressed)];
    [self.mRecordButton initalizeInitialState];
}

- (void) setUpPlayButton
{
    self.mPlayButton.offImage = [UIImage imageNamed:@"Play"];
    self.mPlayButton.onImage = [UIImage imageNamed:@"Pause"];
	[self.mPlayButton subscribeTarget:self withAction:@selector(playPauseButtonPressed)];
}

- (void) setUpEditButton
{
	self.mEditButton.offTitle = @"Edit";
    self.mEditButton.onTitle = @"Done";
    [self.mEditButton subscribeTarget:self withAction:@selector(editButtonPressed)];
}

- (void)setUpSpeakerButton
{
    [self.mSpeakerButton subscribeTarget:self withAction:@selector(speakerButtonPressed)];
}

- (void) setUpDoneButton
{
    self.mDoneButton.hidden = YES;
}

/*====================================================================*/

- (void)setRecordingModeIfNeededTo: (BOOL)value
{
    // Filter recording pause event caused by record button
    if (value == self.mRecordingModeEnabled)
    	return;

    [self setRecordingModeTo:value];
    [self.mVisualizingArea setCursorTimePosition:0];
    [self.mVisualizingArea.amplitudeByTime clear];

    [UIView animateWithDuration:self.mAnimationDuration animations:^{
    	[self.view layoutIfNeeded];
    }];
}

- (void)setRecordingModeTo: (BOOL)value
{
	if (value)
    	self.recordController.currentRecord = [self.recordController createRecord];
    else
    	[self.recordController resetCurrentRecord];
    self.mRecordingModeEnabled = value;

    self.mTableController.enabled = !self.mRecordingModeEnabled;
    self.mEditButton.enabled = !self.mRecordingModeEnabled;

    self.mDoneButton.hidden = !self.mRecordingModeEnabled;
    [self makeVisualizingAreaCollapsed:!self.mRecordingModeEnabled];
}

- (void) makeVisualizingAreaCollapsed: (BOOL)value
{
	[self.mVisualizingArea.constraints objectAtIndex:0].constant =
     		(value ? 0 : kVisualizingAreaHeight);
}

/*====================================================================*/

- (void) setRecordingInProcessStatus: (BOOL) value
{
    self.mVisualizingArea.userInteractionEnabled = !value;
    self.mDoneButton.enabled = !value;
    self.mDoneButton.alpha = value ? 0.5f : 1.0f;
}

- (void) recordButtonPressed
{
    BOOL value = self.mRecordButton.on;
    [self setRecordingModeIfNeededTo:YES];
    [self setRecordingInProcessStatus: value];

    if (value)
        [self.recordController.recorder startRecording:self.recordController.currentRecord];
    else
    	[self.recordController.recorder pauseRecording];
}

- (IBAction) doneButtonPressed:(id)sender
{
	[self.recordController.recorder stopRecording];
    [self.recordController resetCurrentRecord];
    [self setRecordingModeIfNeededTo:NO];
}

- (void) playPauseButtonPressed
{
	if (self.mPlayButton.on)
        [self.recordController.player startPlaying: self.recordController.currentRecord];
    else
    	[self.recordController.player stopPlaying];
}

- (void) editButtonPressed
{
    [self.mTableController setEditing:self.mEditButton.on];
    self.mRecordButton.enabled = !self.mEditButton.on;
}

- (void) speakerButtonPressed
{
    [self.audioSessionController useLoudSpeaker:self.mSpeakerButton.on];
}

/*====================================================================*/

- (void) detailedRecordIsShown: (BOOL)value
{
    self.mEditButton.enabled = !value;
    self.mRecordButton.enabled = !value;
}

- (void) recordWillBecomeSelected: (NSInteger)idx
{
    [self detailedRecordIsShown: YES];
}

- (void) recordWillBecomeDeselected: (NSInteger)idx
{
    [self detailedRecordIsShown: NO];
}

- (void) tableEditingDidRemoveAllRows
{
	self.mEditButton.on = NO;
}

/*====================================================================*/

- (NSInteger)expectedObservingRate
{
    return self.mVisualizingArea.amplitudeByTime.samplesPerSecond;
}

- (void) handleAmplitude: (double)amplitude forTime: (NSTimeInterval)timeInSeconds
{
    dispatch_async(dispatch_get_main_queue(), ^{
    	[self.mVisualizingArea.amplitudeByTime addAmplitude:amplitude forTime:timeInSeconds];
        self.mVisualizingArea.cursorTimePosition = timeInSeconds;
    });
}

- (void)recordingWasInterrupted
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mRecordButton setOnWithoutAnimation:NO];
        [self doneButtonPressed:nil];
    });
}

/*====================================================================*/

- (void)displayError: (NSString *)message
{
    [self.mRecordButton setOnWithoutNotifying:NO];
    [self setRecordingInProcessStatus:NO];
    [self setRecordingModeTo:NO];
    [self.mTableController reset];

    UIAlertController * alert = [UIAlertController
        alertControllerWithTitle:nil message:message
                  preferredStyle:UIAlertControllerStyleAlert
    ];
    UIAlertAction * okAction =
        [UIAlertAction actionWithTitle:@"Ok"
             style:UIAlertActionStyleDefault handler:nil];

    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)displayUnrecoverableError: (NSString *)message
{
   [self displayError:message];
    self.view.userInteractionEnabled = NO;
    self.view.alpha = 0.3f;
}


@end
