//
// Created by Alexey Klimov on 19.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AudioSessionController <NSObject>

- (void) useLoudSpeaker: (BOOL) value;

@end