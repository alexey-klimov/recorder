//
//  RecordCell.h
//  recorder
//
//  Created by Alexey Klimov on 03.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordCell : UITableViewCell

+ (NSString *) identifier;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *time;

@end
