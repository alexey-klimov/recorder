//
// Created by Alexey Klimov on 18.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

@import AVFoundation;

#import <Foundation/Foundation.h>
#import "PlayerRecorderBase.h"

@class Record;

@protocol PlayerObserver

- (void) playerDidStop;
- (void) playerPositionDidChange: (CMTime)value;
- (void) playerFailedToPlay;

@end

// Called only from Main dispatch queue

@interface Player : PlayerRecorderBase

@property (nonatomic) id<PlayerObserver> playerObserver;

- (void) startPlaying: (Record*)record;
- (void) stopPlaying;

@end