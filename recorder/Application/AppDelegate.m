//
//  AppDelegate.m
//  recorder
//
//  Created by Alexey Klimov on 25.05.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

@import AVFoundation;

#import "AppDelegate.h"
#import "RecordController.h"
#import "RootViewController.h"
#import "AudioSessionController.h"
#import "ErrorHandlerPrivate.h"

@interface AppDelegate () <AudioSessionController>

@property (nonatomic) RecordController * mRecordController;
@property (nonatomic) RootViewController * mViewController;
@property (nonatomic) BOOL mGuiPresented;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {

    [self.mRecordController.player interrupt];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    if (self.mGuiPresented)
        return;
    self.mGuiPresented = YES;

    {
        self.mViewController = (RootViewController *)self.window.rootViewController;
        self.mViewController.audioSessionController = self;
        [ErrorHandler sharedInstance].viewController = self.mViewController;
    }

    {
        self. mRecordController = [[RecordController alloc] init];
        NSError *error = [self.mRecordController setup];
        [[ErrorHandler sharedInstance] handleUnrecoverableIfNeeded:error];
        [ErrorHandler sharedInstance].recordController = self.mRecordController;
        
        self.mViewController.recordController = self.mRecordController;
    }

    
    [self configureAudioSession];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    // No way to handle killing app by user. Don't do anything
    // In case of interrupting of recording there will be enough time to finish writing to disk
    [self resetAudioSession];
}

- (void)configureAudioSession
{
    NSError * error = nil;

    {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];

        if ([[ErrorHandler sharedInstance] handleUnrecoverableIfNeeded:error])
            return;
    }
    {
        NSNotificationCenter * c = [NSNotificationCenter defaultCenter];

        [c addObserver:self selector:@selector(audioSessionDidBecomeInterrupted)
            name:AVAudioSessionInterruptionNotification object:nil
        ];

        [c addObserver:self selector:@selector(audioSessionRouteDidBecomeChanged:)
            name:AVAudioSessionRouteChangeNotification object:nil
        ];

        [c addObserver:self selector:@selector(mediaServicesWereReset)
            name:AVAudioSessionMediaServicesWereResetNotification object:nil
        ];
    }

}

- (void)resetAudioSession
{
    NSNotificationCenter * c = [NSNotificationCenter defaultCenter];
    [c removeObserver:self];
}

- (void)audioSessionDidBecomeInterrupted
{
    // Ignores if not running
    [self.mRecordController.recorder interrupt];
    [self.mRecordController.player interrupt];
}

- (void)audioSessionRouteDidBecomeChanged: (NSNotification*)n
{
    AVAudioSessionRouteChangeReason reason = (AVAudioSessionRouteChangeReason)
        ((NSNumber*)n.userInfo[AVAudioSessionRouteChangeReasonKey]).unsignedIntegerValue;

    if (reason == AVAudioSessionRouteChangeReasonNewDeviceAvailable)
    {
        NSLog(@"Headphone/Line plugged in");
        dispatch_async(dispatch_get_main_queue(), ^ {
            self.mViewController.mSpeakerButton.on = NO;
        });
    }else if (reason == AVAudioSessionRouteChangeReasonOldDeviceUnavailable)
    {
        NSLog(@"Headphone/Line was pulled. Stopping player....");
        [self audioSessionDidBecomeInterrupted];
    }
}

- (void)mediaServicesWereReset
{
    [self resetAudioSession];
    [self configureAudioSession];
}

- (void) useLoudSpeaker: (BOOL) value
{
    NSError * error = nil;

    AVAudioSession *session = [AVAudioSession sharedInstance];

    AVAudioSessionPortOverride newValue =
        value ? AVAudioSessionPortOverrideSpeaker : AVAudioSessionPortOverrideNone;
    [session overrideOutputAudioPort:newValue error:&error];
    [[ErrorHandler sharedInstance] handleIfNeeded:error];
}

@end
