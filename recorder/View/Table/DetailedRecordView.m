//
//  DetailedRecordView.m
//  recorder
//
//  Created by Alexey Klimov on 03.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "DetailedRecordView.h"
#import "TwoImageSwitchingButton.h"

@interface DetailedRecordView ()

@property (nonatomic) IBOutlet UIView *mSeparator;
@property (nonatomic) IBOutlet UIView *mMenuSeparator;

@end

@implementation DetailedRecordView

- (IBAction)progressBarPositionChangedByUser {
	//Limit slider at begin or end
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.playOrPauseButton.offImage = [UIImage imageNamed:@"MiniPlayer-play"];
    self.playOrPauseButton.onImage = [UIImage imageNamed:@"MiniPlayer-pause"];
    
    self.playOrPauseButton.imageEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
    
    [self.progressBar setThumbImage:[self generateThumbImageForProgressBar]
    	forState:UIControlStateNormal];
}

- (void) initSeparatorWithColor: (UIColor*)_color
{
	NSArray* separators = @[self.mSeparator, self.mMenuSeparator];

    for (UIView * separator in separators)
    {
        separator.backgroundColor = _color;
        // TODO: That's a hack!
        [separator.constraints objectAtIndex:0].constant =
                1.0f / [UIScreen mainScreen].scale;
        [separator setNeedsUpdateConstraints];
    }
}

- (UIImage*)generateThumbImageForProgressBar
{
	UIImage* templateImg = [UIImage imageNamed:@"MiniPlayer-cursor"];
    
    // Apply system tint color
    UIImage *resultImage = [templateImg imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIGraphicsBeginImageContextWithOptions(templateImg.size, NO, templateImg.scale);
	[self.tintColor set];
	[resultImage drawInRect:CGRectMake(0, 0, templateImg.size.width, templateImg.size.height)];
	resultImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
    return resultImage;
}

@end
