//
// Created by Alexey Klimov on 18.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

@import AVFoundation;

#import "PlayerRecorderBasePrivate.h"
#import "ErrorHandler.h"

@implementation PlayerRecorderBase

- (instancetype)init
{
    if (self = [super init])
    {
        self.running = NO;
    }
    return self;
}

- (Record*)currentRecord
{
    if (!_currentRecord)
        @throw [NSException exceptionWithName:@"NoCurrentRecord" reason:nil userInfo:nil];

    return _currentRecord;
}

- (void) makeAudioSessionActive: (BOOL)value
{
    NSError * error = nil;

    AVAudioSession * session = [AVAudioSession sharedInstance];
    [session setActive:value error:&error];
    if (error)
        @throw [NSException exceptionWithName:error.localizedDescription reason:nil userInfo:nil];
}

- (void) interrupt
{
    NSAssert(NO, @"Not implemented");
}

@end