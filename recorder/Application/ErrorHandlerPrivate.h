//
// Created by Alexey Klimov on 19.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

#import "ErrorHandler.h"
#import "RootViewController.h"
#import "RecordController.h"

@interface ErrorHandler ()

@property (nonatomic) RootViewController * viewController;
@property (nonatomic) RecordController * recordController;

@end