//
//  TitleSwitchButton.h
//  recorder
//
//  Created by Alexey Klimov on 16.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "SwitchButton.h"

@interface TitleSwitchButton : SwitchButton

	@property (nonatomic) NSString * offTitle;
    @property (nonatomic) NSString * onTitle;

@end
