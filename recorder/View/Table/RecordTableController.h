//
//  RecordTableController.h
//  recorder
//
//  Created by Alexey Klimov on 01.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RecordController.h"
#import "Recorder.h"
#import "Player.h"


@class DetailedRecordView;
@class RecordTableView;


@protocol TableStatusObserver

- (void) recordWillBecomeSelected: (NSInteger)index;
- (void) recordWillBecomeDeselected: (NSInteger)index;

- (void) tableEditingDidRemoveAllRows;

@end



@interface RecordTableController : UIViewController
    <UITableViewDataSource, RecorderObserver,
        PlayerObserver, RecordsObserver, TableStatusObserver>

@property (nonatomic) RecordController* recordController;

- (void) setAnimationDuration: (NSTimeInterval)animationDuration;
- (void) setEditing: (BOOL)value;
- (void) setEnabled: (BOOL)value;
- (void) reset;

@end
