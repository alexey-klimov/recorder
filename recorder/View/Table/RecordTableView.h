//
//  RecordTableView.h
//  recorder
//
//  Created by Alexey Klimov on 16.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailedRecordView;
@class RecordTableController;
@class RecordCell;

@interface RecordTableView : UIView

@property (nonatomic) BOOL enabled;
@property (nonatomic) BOOL editing;
@property (nonatomic) NSTimeInterval animationDuration;
@property (nonatomic, readonly) NSInteger selectedRow;

@property (nonatomic) RecordTableController* controller;

- (void) setUpWithDetailedRecordView:(DetailedRecordView*)detailedView;
- (void) reloadData;
- (RecordCell*) cellForRow: (NSInteger)row;
- (void) deleteRow:(NSInteger)row;
- (void)deselectRow;

- (void) reset;

@end
