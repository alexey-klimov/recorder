//
// Created by Alexey Klimov on 18.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

@import AVFoundation;

#import "PlayerRecorderBasePrivate.h"
#import "Player.h"
#import "Record.h"
#import "ErrorHandler.h"

@interface Player ()

@property (nonatomic) AVPlayer * mAVPlayer;
@property (nonatomic) id mPlayerTimeObserver;

@end

@implementation Player

- (void) setupPlayer: (Record*) record
{
    self.initialized = YES;
    [[NSNotificationCenter defaultCenter]
        addObserver:self.playerObserver
           selector:@selector(playerDidStop)
               name:AVPlayerItemDidPlayToEndTimeNotification object:self.mAVPlayer.currentItem
    ];

    self.currentRecord = record;

    [self makeAudioSessionActive:YES];

    self.mAVPlayer = [AVPlayer playerWithURL:self.currentRecord.fileURL];

    // Suppress warning
    __weak Player * weakSelf = self;
    self.mPlayerTimeObserver =
        [self.mAVPlayer addPeriodicTimeObserverForInterval:CMTimeMake(1,10)
                queue:nil usingBlock:^(CMTime time) {
                [weakSelf.playerObserver playerPositionDidChange:time];
            }
        ];

    [self.mAVPlayer.currentItem addObserver:self forKeyPath:@"status"
                   options:NSKeyValueObservingOptionNew context:nil
    ];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *, id> *)change
    context:(void *)context
{
    if (self.mAVPlayer.currentItem.status == AVPlayerStatusFailed)
    {
        [self.playerObserver playerFailedToPlay];
        [[ErrorHandler sharedInstance] handleIfNeeded:self.mAVPlayer.currentItem.error];
        [self resetPlayer];
    }
    else if (self.mAVPlayer.currentItem.status == AVPlayerStatusReadyToPlay)
    {
        NSLog(@"start playing");
        [self.mAVPlayer play];
        self.running = YES;
    }
}

- (void) resetPlayer
{
    self.initialized = NO;
    self.running = NO;

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.mAVPlayer removeTimeObserver:self.mPlayerTimeObserver];
    [self.mAVPlayer.currentItem removeObserver:self forKeyPath:@"status"];
    self.mPlayerTimeObserver = nil;
    self.mAVPlayer = nil;
}

- (void) startPlaying: (Record*) record
{
    if (self.initialized)
        return;

    @try
    {
        [self setupPlayer:record];
    }
    @catch (NSException * e)
    {
        [[ErrorHandler sharedInstance] handleUnrecoverable:e.name];
        [self resetPlayer];
    }
}

- (void) stopPlaying
{
    if (!self.initialized)
        return;
    
    [self.mAVPlayer pause];
    [self resetPlayer];
    NSLog(@"stop playing");
    
    @try
    {
        [self makeAudioSessionActive:NO];
    }
    @catch (NSException * e)
    {
        [[ErrorHandler sharedInstance] handle:e.name];
    }
}

- (void) interrupt
{
    if (self.running)
        [self.playerObserver playerDidStop];
}

@end