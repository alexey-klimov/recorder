# Recoder app #

### What is this repository for? ###

This project is intended to demonstrate my knowledge of iOS platform and general coding skills.
It reimplements a subset of features of iOS system app "Voice Memos" as well as uses its UI design.

### Implemented features: ###
* Visualizing area that displays live PCM of sound being recorded.
* Good-looking and animated record button
* Table with custom selection animation
* Mini-player in detailed record view
* Recording with proper reaction to audio session interruption or app going to background
* Playing with proper reaction to headset plug in/out
* Switching between loud speaker and default one

### Demonstration ###
#### Videos ####
[Video demonstration](https://youtu.be/7IgZFWeO8Hc)

#### Screenshots ####

![IMG_0013.PNG](https://bitbucket.org/repo/ra8LjK/images/2399672557-IMG_0013.PNG)
![IMG_0012.PNG](https://bitbucket.org/repo/ra8LjK/images/4061524706-IMG_0012.PNG)
![IMG_0014.PNG](https://bitbucket.org/repo/ra8LjK/images/26261566-IMG_0014.PNG)