//
//  RecordButton.m
//  recorder
//
//  Created by Alexey Klimov on 16.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "RecordButton.h"

@interface RecordButton ()

@property (nonatomic) UIImage * backgroundSource;
@property (nonatomic) CALayer * background;

@property (nonatomic) CGRect offRect;
@property (nonatomic) CGFloat offCornerRadius;
@property (nonatomic) UIEdgeInsets offImageInsets;

@property (nonatomic) CGRect onRect;
@property (nonatomic) CGFloat onCornerRadius;
@property (nonatomic) UIEdgeInsets onImageInsets;

@end

@implementation RecordButton

-(void)setEnabled:(BOOL)enabled
{
	[super setEnabled:enabled];
    self.background.opacity = enabled ? 1.0f : 0.5f;
}

- (void)initalizeInitialState
{
	self.backgroundSource = [UIImage imageNamed:@"Record-background"];
    
    CGFloat offImageSize = self.bounds.size.height * 0.75f;
    self.offCornerRadius = offImageSize / 2;
    CGFloat offInset = (self.bounds.size.height - offImageSize) / 2;
    self.offImageInsets = UIEdgeInsetsMake(offInset, offInset, offInset, offInset);
    
    CGFloat onImageSize = self.bounds.size.height * 0.35f;
    self.onCornerRadius = onImageSize / 8;
    CGFloat onInset = (self.bounds.size.height - onImageSize) / 2;
    self.onImageInsets = UIEdgeInsetsMake(onInset, onInset, onInset, onInset);
	
    
    self.background = [[CALayer alloc]init];
    self.background.frame = self.layer.bounds;
    self.background.contents = (__bridge id)[self.backgroundSource CGImage];
    [self.layer addSublayer:self.background];
    
    self.imageEdgeInsets = self.offImageInsets;
    self.imageView.layer.cornerRadius = self.offCornerRadius;
}

- (void) setOn: (BOOL)value
{
	[super setOn: value];
	self.userInteractionEnabled = NO;
    
    CALayer* layer = self.imageView.layer;
	layer.cornerRadius =  value ? self.onCornerRadius : self.offCornerRadius;
    CABasicAnimation * radiusAnimation = [[CABasicAnimation alloc] init];
    radiusAnimation.fromValue =
    	@( value ? self.offCornerRadius : self.onCornerRadius );
    radiusAnimation.toValue = @(layer.cornerRadius);
    radiusAnimation.duration = self.animationDuration;
    [layer addAnimation:radiusAnimation forKey:@"cornerRadius"];
    
    [UIView animateWithDuration:self.animationDuration
    	animations:^{
        	self.imageEdgeInsets
            	= value ? self.onImageInsets : self.offImageInsets;
            [self layoutIfNeeded];
        }
        completion:^(BOOL done){ if (done){
        	[layer removeAnimationForKey:@"cornerRadius"];
            self.userInteractionEnabled = YES;
        }}
    ];
}

- (void)setOnWithoutAnimation:(BOOL)value
{
    [UIView performWithoutAnimation:^{ [self setOn:value]; }];
}

@end
