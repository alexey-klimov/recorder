//
//  RootViewController.h
//  recorder
//
//  Created by Alexey Klimov on 25.05.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RecordTableController.h"
#import "AudioSessionController.h"
#import "TintColorSwitchButton.h"

@class RecordController;
@interface RootViewController : UIViewController <TableStatusObserver>

@property (nonatomic) RecordController* recordController;
@property (nonatomic) id<AudioSessionController> audioSessionController;
@property (weak, nonatomic) IBOutlet TintColorSwitchButton*  mSpeakerButton;

- (void)displayError: (NSString*)message;
- (void)displayUnrecoverableError: (NSString*)message;

@end

