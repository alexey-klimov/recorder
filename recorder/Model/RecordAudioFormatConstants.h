#import <CoreAudio/CoreAudioTypes.h>
#import <AVFoundation/AVMediaFormat.h>

extern NSNumber * kRCOutputAudioStreamFormat;
extern NSNumber * KRCOutputAudioSampleRate;
extern NSNumber * KRCOutputAudioNumberOfChannels;
extern NSString * kRCAudioFileFormat;
extern NSString * kRCAudioFileFormatExtension;