//
//  Record.m
//  recorder
//
//  Created by Alexey Klimov on 17.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import "Record.h"

@interface Record ()

@end

@implementation Record

- (instancetype) initWithURL:(NSURL*)fileURL
{
	if (self = [super init])
    {
    	_fileURL = fileURL;
    	[self setUpName];
        [self updateCreationTime];
    }
    return self;
}

- (void) setUpName
{
	NSString * nameWithExtension = _fileURL.lastPathComponent;
    _name = nameWithExtension.stringByDeletingPathExtension;
}

- (void) updateCreationTime
{
	NSFileManager * fm = [NSFileManager defaultManager];
    NSError* error = nil;
    NSDictionary* dict = [fm attributesOfItemAtPath:_fileURL.path error:&error];
    if (!error)
    {
    	_creationDate = dict.fileModificationDate;
    }
    else
    {
    	// Only possible when creating new record while
	    // actual capturing hasn't started yet
    	_creationDate = [NSDate dateWithTimeIntervalSinceNow:0];
    }
}

- (NSTimeInterval)recordedLength
{
	if ([_fileURL checkResourceIsReachableAndReturnError:nil])
    {
    	AVAsset * asset = [AVAsset assetWithURL:_fileURL];
    	return (NSTimeInterval)asset.duration.value / asset.duration.timescale;
    }
    else
    	return 0.0;
}

@end
