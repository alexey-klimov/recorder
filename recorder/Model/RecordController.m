//
//  RecordController.m
//  recorder
//
//  Created by Alexey Klimov on 17.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "RecordAudioFormatConstants.h"


#import "RecordController.h"
#import "Record.h"
#import "Recorder.h"
#import "Player.h"
#import "ErrorHandler.h"

@interface RecordController ()

@property (nonatomic) NSMutableArray *mRecords;
@property (nonatomic) NSFileManager *mFileManager;
@property (nonatomic) NSURL *mStoringDir;

@end

@implementation RecordController

/*====================================================================*/

- (NSError*) setup
{
    @try
    {
        [self setUpRecordsManagement];
        [self restoreRecords];
        _recorder = [[Recorder alloc] init];
        _player = [[Player alloc] init];

        return nil;
    }
    @catch (NSException * e)
    {
        return [NSError errorWithDomain:NSCocoaErrorDomain code:0 userInfo:nil];
    }
}



- (void) setUpRecordsManagement
{
	self.mFileManager = [NSFileManager defaultManager];
    self.mStoringDir = [self getRecordsStoringDirectory];

    self.mRecords = [[NSMutableArray alloc] init];
    [self setCurrentRecord:nil];
    
    NSLog(@"Records storing dir: %@", self.mStoringDir.path);
}

- (void) restoreRecords
{
	NSDirectoryEnumerator * en = [self.mFileManager enumeratorAtURL:self.mStoringDir
    	includingPropertiesForKeys:nil
        options:NSDirectoryEnumerationSkipsSubdirectoryDescendants
        errorHandler:nil
	];

    for (NSURL * file in en)
    {
        if ([file.pathExtension isEqualToString:kRCAudioFileFormatExtension])
            [self _newRecord:file];
    }
}

/*====================================================================*/

- (BOOL) ensureDirectoryExists: (NSURL*)directory
{
	NSError * error = nil;
    BOOL result = [self.mFileManager createDirectoryAtPath:[directory path]
        withIntermediateDirectories:YES attributes:nil error:&error
    ];

    if (error)
        @throw [NSException exceptionWithName:@"RecordDirectoryCreation" reason:nil userInfo:nil];
    return result;

}

- (NSURL*) getRecordsStoringDirectory
{
    NSURL* documents = [self.mFileManager
        URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask
    ][0];
    
    NSURL* recordsStroingDir = documents;
    [self ensureDirectoryExists:recordsStroingDir];
    
    return recordsStroingDir;
}

/*====================================================================*/

- (BOOL) isRecordNameUnique: (NSString*)name
{
	return [self makeURLIfRecordNameUnique:name] != nil;
}

- (nullable NSURL*) makeURLIfRecordNameUnique: (NSString*)name
{
	NSString * fileName = [name stringByAppendingPathExtension:kRCAudioFileFormatExtension];
    NSURL * filePath = [self.mStoringDir URLByAppendingPathComponent:fileName];
    
    return ![self.mFileManager fileExistsAtPath:filePath.path] ? filePath : nil;
}

- (NSURL*) generateUniqueName
{
	// TODO: Make localized
    static NSString * const nameTemplate = @"NewRecord%ld";
    
    long counter = 1;
    while (true)
    {
        NSString * recordName = [NSString stringWithFormat:nameTemplate, counter];
        NSURL* fileURL = [self makeURLIfRecordNameUnique:recordName];
        if ( fileURL )
        	return fileURL;
        ++counter;
    }
}

/*====================================================================*/

- (NSInteger) recordsCount
{
	return (NSInteger)self.mRecords.count;
}

- (Record*) getRecordByIndex: (NSInteger)index
{
	return self.mRecords[(NSUInteger) index];
}

/*====================================================================*/

- (Record *) _newRecord: (NSURL*)file
{
    Record * record = [[Record alloc] initWithURL:file];
    [self.mRecords addObject:record];
    return record;
}

- (Record*) createRecord
{
	Record* record = [self _newRecord:[self generateUniqueName]];
    [self.recordsObserver recordWasCreated: self.recordsCount];
    return record;
}

- (void) removeRecordAtIndex: (NSInteger)index
{
	Record* record = [self getRecordByIndex:index];

    NSError * error = nil;

    [self.mFileManager removeItemAtURL:record.fileURL error:&error];
    [self.mRecords removeObjectAtIndex:(NSUInteger)index];

    [[ErrorHandler sharedInstance] handleIfNeeded:error];
}

- (void) reload
{
    [self.mRecords removeAllObjects];
    [self restoreRecords];
    [self.recordsObserver recordsWasReloaded];
}

/*====================================================================*/

- (void) setCurrentRecord:(Record *)currentRecord
{
	_currentRecord = currentRecord;

    if (!_currentRecord)
    {
        _currentRecordId = CURRENT_RECORD_NOT_INTIALIZED;
        return;
    }

    for (int i = 0; i < self.recordsCount; ++i)
    {
    	if (currentRecord == [self getRecordByIndex:i])
        {
        	_currentRecordId = i;
            return;
        }
    }
}

- (void) resetCurrentRecord
{
	self.currentRecord = nil;
}

/*====================================================================*/

@end
