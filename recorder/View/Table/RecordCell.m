//
//  RecordCell.m
//  recorder
//
//  Created by Alexey Klimov on 03.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "RecordCell.h"

@implementation RecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (NSString *) identifier
{
    return @"RecordCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
