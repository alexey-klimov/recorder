//
//  InactiveLayerView.m
//  recorder
//
//  Created by Alexey Klimov on 01.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "InactiveLayerView.h"

@interface InactiveLayerView ()
    @property (nonatomic) UIImageView* mSnapshot;
    @property (nonatomic) UIView* mGrayLayer;
@end

@implementation InactiveLayerView

- (void) initSnapshotView
{
	self.mSnapshot = [[UIImageView alloc] initWithImage:nil];
    self.mSnapshot.frame = self.bounds;
    self.mSnapshot.hidden = NO;
    self.mSnapshot.contentMode = UIViewContentModeScaleAspectFill;
    
    self.mSnapshot.autoresizingMask = UIViewAutoresizingFlexibleWidth
        | UIViewAutoresizingFlexibleHeight;
    self.mSnapshot.translatesAutoresizingMaskIntoConstraints = YES;
    
    [self addSubview:self.mSnapshot];
}

- (void) initGrayLevelView
{
	self.mGrayLayer = [[UIView alloc] initWithFrame:self.bounds];
    
    self.mGrayLayer.autoresizingMask = UIViewAutoresizingFlexibleWidth
    	| UIViewAutoresizingFlexibleHeight;
    self.mGrayLayer.translatesAutoresizingMaskIntoConstraints = YES;
    
    self.mGrayLayer.backgroundColor = [UIColor grayColor];
    self.mGrayLayer.alpha = 0;
    [self addSubview:self.mGrayLayer];
}

- (void) initGestureRecognizer
{
	UITapGestureRecognizer * gc = [[UITapGestureRecognizer alloc] init];
    [self addGestureRecognizer: gc];
}

- (id)initWithFrame:(CGRect)aRect {
    if (self = [super initWithFrame:aRect])
    {
        [self initSnapshotView];
        [self initGrayLevelView];
        [self initGestureRecognizer];
    }
    
    return self;
}

- (CGRect) frameInPixels
{
	CGFloat scale = [UIScreen mainScreen].scale;
	CGRect frame;
    frame.origin.x = self.frame.origin.x * scale;
    frame.origin.y = self.frame.origin.y * scale;
    frame.size.height = self.frame.size.height * scale;
    frame.size.width = self.frame.size.width * scale;
    
    return frame;
}

- (void) captureUnderneathContentImage
{
    if (self.frame.size.height == 0)
    	return;
    UIGraphicsBeginImageContextWithOptions(self.superview.bounds.size, YES, 0);
	[self.superview drawViewHierarchyInRect: self.superview.bounds afterScreenUpdates:NO];
	UIImage* img = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

    CGImageRef ref = CGImageCreateWithImageInRect(img.CGImage, [self frameInPixels]);
    NSAssert(ref, @"Should be initialized");
    
    UIImage * selfFrameSnapshot = [UIImage imageWithCGImage:ref];
    CFRelease(ref);
    self.mSnapshot.image = selfFrameSnapshot;
}

- (void) becomeInactive
{
    self.hidden = YES;
    [UIView animateWithDuration:self.animationDuration
                     animations:^{ self.mGrayLayer.alpha = 0.3f; }];
}

- (void) becomeActive
{
    [UIView animateWithDuration:self.animationDuration
                     animations:^{ self.mGrayLayer.alpha = 0; }];
}

- (void) reset
{
    self.mSnapshot.image = nil;
    self.hidden = YES;
}

@end
