//
//  RecordButton.h
//  recorder
//
//  Created by Alexey Klimov on 16.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "SwitchButton.h"

@interface RecordButton : SwitchButton

@property (nonatomic) NSTimeInterval animationDuration;

- (void)initalizeInitialState;
- (void)setOnWithoutAnimation:(BOOL)on;

@end
