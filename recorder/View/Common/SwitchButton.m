//
//  SwitchButton.m
//  recorder
//
//  Created by Alexey Klimov on 15.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "SwitchButton.h"

@interface Subscriber : NSObject

@property (nonatomic) id target;
@property (nonatomic) SEL action;

@end

@implementation Subscriber
@end

@interface SwitchButton ()

@property (nonatomic) NSMutableArray* mSubscribers;
@property (nonatomic) BOOL mShouldNofify;

@end


@implementation SwitchButton

-(void)subscribeTarget:(nonnull id)target withAction:(nonnull SEL)action
{
	Subscriber* s = [[Subscriber alloc] init];
    s.target = target;
    s.action = action;
    [self.mSubscribers addObject:s];
}

- (id) initWithFrame:(CGRect)frame
{
	if (self = [super initWithFrame:frame])
    {
    	[self initialize];
    }
    return self;
}
- (id) initWithCoder:(NSCoder*)coder
{
	if (self = [super initWithCoder:coder])
    {
    	[self initialize];
    }
    return self;
}

- (void) initialize
{
	_on = NO;
    self.mShouldNofify = YES;
    self.mSubscribers = [NSMutableArray array];
    // Target-actions are stored in hashtable so there are actually no garantees
    // that it would be called BEFORE any other. There may be situation when other
    // code works with incorrect "on" property
    [self addTarget:self action:@selector(switchState) forControlEvents:UIControlEventTouchUpInside];
}

- (void) setOnWithoutNotifying: (BOOL)value
{
    self.mShouldNofify = NO;
    self.on = value;
    self.mShouldNofify = YES;
}

- (void) setOn: (BOOL)value
{
    _on = value;
    if (!self.mShouldNofify)
        return;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    for (Subscriber* s in self.mSubscribers)
        [s.target performSelector:s.action];
#pragma clang diagnostic pop

}

- (void)switchState
{
   	self.on = !self.on;
}

@end
