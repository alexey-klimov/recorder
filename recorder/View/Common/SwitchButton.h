//
//  SwitchButton.h
//  recorder
//
//  Created by Alexey Klimov on 15.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
This should've been done via UIButton's states.
But unfortunately there are some not solved problems:
	- There is unwanted highlighting for button rect in selected state if
	system button style is used
	- If custom button style is used, transition "selected->normal" is caused by
    touch down instead of touch inside up.
*/

@interface SwitchButton : UIButton

- (void) setOnWithoutNotifying: (BOOL)value;
@property (nonatomic) BOOL on;

- (void)initialize;

-(void)subscribeTarget:(nonnull id)target withAction:(nonnull SEL)action;

@end
