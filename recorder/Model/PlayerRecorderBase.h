//
// Created by Alexey Klimov on 18.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

#import <Foundation/Foundation.h>

/*============================================================================*/

@interface PlayerRecorderBase : NSObject

@property (nonatomic, readonly) BOOL running;
// Ignores if not running
- (void) interrupt;

@end

/*============================================================================*/