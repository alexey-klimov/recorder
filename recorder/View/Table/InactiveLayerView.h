//
//  InactiveLayerView.h
//  recorder
//
//  Created by Alexey Klimov on 01.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InactiveLayerView : UIView

@property (nonatomic) NSTimeInterval animationDuration;

- (void) captureUnderneathContentImage;
- (void) becomeInactive;
- (void) becomeActive;

- (void) reset;

@end
