//
//  TwoImageSwitchingButton.h
//  recorder
//
//  Created by Alexey Klimov on 16.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//


#import "SwitchButton.h"

@interface TwoImageSwitchingButton : SwitchButton

@property (nonatomic) UIImage * offImage;
@property (nonatomic) UIImage * onImage;

@end
