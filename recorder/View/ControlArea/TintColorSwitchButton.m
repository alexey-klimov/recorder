//
//  TintColorSwitchButton.m
//  recorder
//
//  Created by Alexey Klimov on 16.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "TintColorSwitchButton.h"

@interface TintColorSwitchButton ()

@property (nonatomic) UIColor * offColor;
@property (nonatomic) UIColor * onColor;

@end

@implementation TintColorSwitchButton

- (void) initialize
{
	[super initialize];
    self.onColor = self.tintColor;
    self.offColor = [UIColor whiteColor];
    self.on = NO;
}

- (void) setOn: (BOOL)value
{
	[super setOn: value];
    self.tintColor = value ? self.onColor : self.offColor;
}


@end
