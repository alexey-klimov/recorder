//
//  RecordController.h
//  recorder
//
//  Created by Alexey Klimov on 17.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

@class Record;
@class Recorder;
@class Player;

/*============================================================================*/

@protocol RecordsObserver <NSObject>

 - (void) recordWasCreated: (NSInteger)recordIndex;
 - (void) recordsWasReloaded;

@end

/*============================================================================*/

const static NSInteger CURRENT_RECORD_NOT_INTIALIZED = -1;

// Called only from Main dispatch queue

@interface RecordController : NSObject

@property (readonly, nonatomic) Recorder * recorder;
@property (readonly, nonatomic) Player * player;

@property (nonatomic) id<RecordsObserver> recordsObserver;
@property (readonly, nonatomic) NSInteger recordsCount;

- (NSError*) setup;
- (void) reload;

- (Record*) createRecord;
- (Record*) getRecordByIndex: (NSInteger)index;
- (void) removeRecordAtIndex: (NSInteger)index;

@property (nonatomic) Record* currentRecord;

// Returns CURRENT_RECORD_NOT_INTIALIZED if no current record set
@property (nonatomic, readonly) NSInteger currentRecordId;
- (void) resetCurrentRecord;

@end

/*============================================================================*/