//
// Created by Alexey Klimov on 18.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerRecorderBase.h"

@class Record;

@interface PlayerRecorderBase ()

@property (nonatomic) Record * currentRecord;
- (void) makeAudioSessionActive: (BOOL)value;
@property (nonatomic) BOOL running;
@property (nonatomic) BOOL initialized;


@end