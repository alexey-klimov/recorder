//
//  RecordTableController.m
//  recorder
//
//  Created by Alexey Klimov on 01.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import <CoreMedia/CoreMedia.h>

#import "RecordTableController.h"
#import "RecordCell.h"
#import "RecordTableView.h"
#import "DetailedRecordView.h"
#import "TwoImageSwitchingButton.h"

#import "Record.h"

@interface RecordTableController ()

@property (nonatomic) IBOutlet DetailedRecordView *mDetailedView;
@property (nonatomic) IBOutlet RecordTableView *mTableView;

@property (nonatomic) id<TableStatusObserver> mTableStatusObserver;
@property (nonatomic) NSInteger kProgressBarDensityMultiplier;

@end

@implementation RecordTableController

- (void) viewDidLoad
{
    [super viewDidLoad];

    [self setUp];
}

- (void) reset
{
    [self.mTableView reset];
}

- (void) setUp
{
    [self.mDetailedView.removeButton addTarget:self
        action:@selector(removeButtonPressed)
        forControlEvents:UIControlEventTouchUpInside];

    [self.mDetailedView.playOrPauseButton subscribeTarget:self
        withAction:@selector(playPauseButtonPressed)
    ];
    self.mDetailedView.progressBar.userInteractionEnabled = NO;

    [self.mTableView setUpWithDetailedRecordView:self.mDetailedView];
    self.mTableView.controller = self;

    self.kProgressBarDensityMultiplier = 1000;
}

- (void) setRecordController: (RecordController*)rc
{
    _recordController = rc;

    self.recordController.recordsObserver = self;
    self.recordController.recorder.recorderObserver = self;
    self.recordController.player.playerObserver = self;
}

- (void) setAnimationDuration: (NSTimeInterval) animationDuration
{
	self.mTableView.animationDuration = animationDuration;
}

- (void) setEditing: (BOOL)value
{
	self.mTableView.editing = value;
}

- (void) setEnabled: (BOOL)value
{
	self.mTableView.enabled = value;
}

- (id<TableStatusObserver>)mTableStatusObserver
{
    return (id<TableStatusObserver>)self.parentViewController;
}

/*====================================================================*/

- (void)playPauseButtonPressed
{
    if (self.mDetailedView.playOrPauseButton.on)
    	[self.recordController.player startPlaying:self.recordController.currentRecord];
    else
    	[self.recordController.player stopPlaying];
}

- (void)removeButtonPressed
{
    UIAlertController * alert = [UIAlertController
    	alertControllerWithTitle:nil message:nil
        preferredStyle:UIAlertControllerStyleActionSheet
    ];
    UIAlertAction * removeAction = [UIAlertAction actionWithTitle:@"Remove"
    	style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            [self deleteRow:self.mTableView.selectedRow];

            if (self.mDetailedView.playOrPauseButton.on)
                self.mDetailedView.playOrPauseButton.on = NO;
        }];

    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
    	style:UIAlertActionStyleCancel handler:nil];

    [alert addAction:removeAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

/*====================================================================*/

- (void) setMiniPlayerTimeViewsForTime: (CMTime)currentTime
		 withLength: (NSTimeInterval)length
{
    NSInteger seconds = (NSInteger)(currentTime.value / currentTime.timescale);
    self.mDetailedView.progressBar.value =
    	self.kProgressBarDensityMultiplier * currentTime.value / currentTime.timescale;
    self.mDetailedView.playerTime.text = [self shortStringFromTime:seconds];
    self.mDetailedView.playerTimeLeft.text =
    	[self shortStringFromTime:(length - seconds)];
}

- (void) fillDetailedRecordViewWithRecordId:(NSInteger)index
{
    Record* record = [self.recordController getRecordByIndex:index];
	self.recordController.currentRecord = record;

	self.mDetailedView.name.text = record.name;
    self.mDetailedView.date.text = [self stringFromDate:record.creationDate];

   	NSString* lengthString =
    	[self stringFromTime:record.recordedLength];

    self.mDetailedView.time.text = lengthString;

    self.mDetailedView.progressBar.minimumValue = 0;
    self.mDetailedView.progressBar.maximumValue =
    	(float)record.recordedLength * self.kProgressBarDensityMultiplier;

    [self setMiniPlayerTimeViewsForTime:CMTimeMake(0,1)
        withLength:record.recordedLength];
}

- (NSString*) stringFromDate: (NSDate*)date
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd.MM.yy"];

	 return [formatter stringFromDate:date];
}

- (NSString*) stringFromTime: (NSTimeInterval) length
{
    NSInteger hours = (NSInteger)length / 3600;
    NSInteger minutes = ((NSInteger)length % 3600) / 60;
    NSInteger seconds = ((NSInteger)length % 3600) % 60;

    return [NSString stringWithFormat:@"%.2ld:%.2ld:%.2ld",
        (long)hours, (long)minutes, (long)seconds
    ];
}

- (NSString*) shortStringFromTime: (NSTimeInterval) length
{
    NSInteger minutes = ((NSInteger)length % 3600) / 60;
    NSInteger seconds = ((NSInteger)length % 3600) % 60;

    return [NSString stringWithFormat:@"%ld:%.2ld", (long)minutes, (long)seconds];
}

/*====================================================================*/

- (void) tableEditingDidRemoveAllRows
{
}

- (void) recordWillBecomeSelected: (NSInteger)index
{
	[self fillDetailedRecordViewWithRecordId:index];
    [self.mTableStatusObserver recordWillBecomeSelected:index];
}

- (void) recordWillBecomeDeselected: (NSInteger)index
{
	[self.mTableStatusObserver recordWillBecomeDeselected:index];
    if (self.mDetailedView.playOrPauseButton.on)
        self.mDetailedView.playOrPauseButton.on = NO;
}

/*====================================================================*/

- (NSInteger)tableView:(UITableView *)tableView
	numberOfRowsInSection:(NSInteger)section
{
    return self.recordController.recordsCount;
}

- (UITableViewCell *) workaroundForReloadData: (UITableView *)tableView
{
    // Problem: UITableView::-reloadData uses dataSourceDelegate's info in
    // two steps: during reloadData call it gets number of cells and then
    // asks for rows during layoutSubviews. It makes this operation nonatomic and
    // there is no way for other tasks to know that they don't interrupt table
    // reloading process. This workaround is the fastest and the simplest way to deal with it.
    // This way makes model access safe for table view, preventing from out of bounds errors.

    UITableViewCell* cell =
        [tableView dequeueReusableCellWithIdentifier:@"DummyCell"];


    // Don't create copies for constraint when cell is reused
    if (cell.contentView.subviews[0].constraints.count == 0)
    {
        [cell.contentView.subviews[0].heightAnchor
                constraintEqualToConstant:tableView.estimatedRowHeight].active = YES;
    }

    return cell;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
	cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row >= self.recordController.recordsCount)
        return [self workaroundForReloadData:tableView];

    Record* record = [self.recordController getRecordByIndex:indexPath.row];

    RecordCell* cell =
    	[tableView dequeueReusableCellWithIdentifier:@"RecordCell"];
    cell.name.text = record.name;
    cell.date.text = [self stringFromDate:record.creationDate];
    cell.time.text = [self stringFromTime:record.recordedLength];

    return cell;
}

- (void) tableView:(UITableView *)tableView
	commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
    forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
	[self deleteRow:indexPath.row];
    if (!self.recordController.recordsCount)
    	[self.mTableStatusObserver tableEditingDidRemoveAllRows];

}

- (void) deleteRow: (NSInteger)row
{
	[self.recordController removeRecordAtIndex:row];
    [self.mTableView deleteRow:row];
}


/*====================================================================*/

- (void) playerDidStop
{
    dispatch_async(dispatch_get_main_queue(), ^{

        self.mDetailedView.playOrPauseButton.on = NO;

    });
}

- (void) recordedDurationDidChange: (NSInteger)value
{
    dispatch_async(dispatch_get_main_queue(), ^{

        RecordCell *cell = [self.mTableView cellForRow:
            self.recordController.currentRecordId
        ];
        if (!cell)
            return;

        cell.time.text = [self stringFromTime:value];

    });
}

- (void) playerPositionDidChange: (CMTime)value
{
    dispatch_async(dispatch_get_main_queue(), ^{

        if (self.mDetailedView.hidden)
            return;
        [self setMiniPlayerTimeViewsForTime:value
            withLength:self.recordController.currentRecord.recordedLength
        ];

    });
}

- (void) playerFailedToPlay
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mDetailedView.playOrPauseButton setOnWithoutNotifying:NO];
    });
}

- (void) recordWasCreated: (NSInteger)recordIndex
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mTableView reloadData];
    });
}

- (void)recordsWasReloaded
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mTableView reloadData];
    });
}

/*====================================================================*/
@end
