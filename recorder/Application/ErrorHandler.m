//
// Created by Alexey Klimov on 19.07.16.
// Copyright (c) 2016 Demonstration. All rights reserved.
//

#import "ErrorHandlerPrivate.h"

static ErrorHandler * gErrorHandler = nil;

@implementation ErrorHandler

+ (instancetype) sharedInstance
{
    if (!gErrorHandler)
        gErrorHandler = [[ErrorHandler alloc] init];
    return gErrorHandler;
}

- (BOOL) handleIfNeeded: (NSError*)error
{
    if (error)
        [self handle:error.localizedDescription];
    return error == nil;
}

- (BOOL) handleUnrecoverableIfNeeded: (NSError*)error
{
    if (error)
        [self handleUnrecoverable:error.localizedDescription];
    return error != nil;
}

- (void) handle: (NSString*)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.viewController displayError:message];
        
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.recordController reload];
    });
}

- (void)handleUnrecoverable:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.viewController displayUnrecoverableError:message];
    });
}

@end