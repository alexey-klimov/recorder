//
//  TwoImageSwitchingButton.m
//  recorder
//
//  Created by Alexey Klimov on 16.06.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

#import "TwoImageSwitchingButton.h"

@implementation TwoImageSwitchingButton

- (void) setOn: (BOOL)value
{
	[super setOn: value];
    [self setImage:(value ? self.onImage : self.offImage)
    	forState:UIControlStateNormal];
}

@end
