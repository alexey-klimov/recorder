//
//  Recorder+RecordingFacilities.m
//  recorder
//
//  Created by Alexey Klimov on 08.07.16.
//  Copyright © 2016 Demonstration. All rights reserved.
//

@import AVFoundation;

#import "Recorder.h"
#import "PlayerRecorderBasePrivate.h"
#import "Record.h"
#import "RecordAudioFormatConstants.h"
#import "ErrorHandler.h"

@interface Recorder () <AVCaptureAudioDataOutputSampleBufferDelegate>

@property (nonatomic) BOOL mIsPaused;
@property (nonatomic) NSInteger mStreamObserverNotifyingRate;
@property (nonatomic) AVCaptureDevice * mCaptureDevice;
@property (nonatomic) AudioStreamBasicDescription* mDeviceASBD;

@property (nonatomic) AVCaptureSession * mCaptureSession;
@property (nonatomic) AVCaptureInput * mCaptureInput;
@property (nonatomic) AVCaptureAudioDataOutput * mCaptureOutput;
@property (nonatomic) AVAssetWriter * mAssetWriter;
@property (nonatomic) CMTime mCurrentBufferTime;
@property (nonatomic) dispatch_queue_t mRecorderQueue;

@end

static inline void RCFail(BOOL cond, NSString *message)
{
    if (!cond)
        @throw [NSException exceptionWithName:message reason:nil userInfo:nil];
}

@implementation Recorder

/*============================================================================*/

- (void) setUpRecorder: (Record*)record
{
    self.initialized = YES;

    self.currentRecord = record;
    [self makeAudioSessionActive:YES];
    NSError* error = nil;

    // Capture device
    self.mCaptureDevice =
    	[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    RCFail(self.mCaptureDevice != nil, @"Mic should be available.");

    // Capture input
    self.mCaptureInput =
    	[AVCaptureDeviceInput deviceInputWithDevice:self.mCaptureDevice error:&error];
    RCFail(self.mCaptureInput != nil, @"Device's port configuration failed");

    // Capture output
    self.mRecorderQueue = dispatch_queue_create(NULL, DISPATCH_QUEUE_SERIAL);
    self.mCaptureOutput = [[AVCaptureAudioDataOutput alloc] init];
    [self.mCaptureOutput setSampleBufferDelegate:self queue:self.mRecorderQueue];
    
    // Capture session
    self.mCaptureSession = [[AVCaptureSession alloc] init];
    [self.mCaptureSession addInput:self.mCaptureInput];
    [self.mCaptureSession addOutput:self.mCaptureOutput];

    // Stream observing rate
    self.mStreamObserverNotifyingRate =
        self.recordedAudioStreamObserver.expectedObservingRate;
}

// Sets up asset writer, frame time counter and
// ensures that audio stream format is supported.
// Called via dispatch queue from nonmain thread.
- (void) setUpAudioStreamFormatDependentPartIfNeeded: (CMSampleBufferRef)sampleBuffer
{
    if (self.mDeviceASBD)
        return;

    CMFormatDescriptionRef formatDescription =
        CMSampleBufferGetFormatDescription(sampleBuffer);
    
    const AudioStreamBasicDescription * ASBD =
        CMAudioFormatDescriptionGetStreamBasicDescription(formatDescription);

    self.mDeviceASBD = malloc(sizeof(AudioStreamBasicDescription));
    memcpy(self.mDeviceASBD, ASBD, sizeof(AudioStreamBasicDescription));
    
    [self setUpAssetWriter];
    [self.mAssetWriter startWriting];
    [self.mAssetWriter startSessionAtSourceTime:kCMTimeZero];
    
    self.mCurrentBufferTime = CMTimeMake(0, (int32_t)self.mDeviceASBD->mSampleRate);
    
    // Ensure settings are supported
    RCFail (self.mDeviceASBD->mFormatID == kAudioFormatLinearPCM,
        @"Unsupported mic device: output stream format"
    );

    RCFail(self.mDeviceASBD->mChannelsPerFrame == 1,
        @"Unsupported mic device: number of channels"
    );

    RCFail(((self.mDeviceASBD->mBytesPerFrame == 1)
        ||    (self.mDeviceASBD->mBytesPerFrame == 2)
        ||    (self.mDeviceASBD->mBytesPerFrame == 4)
        ||    (self.mDeviceASBD->mBytesPerFrame == sizeof(double))),
        @"Unsupported mic device: output stream depth"
    );
}

- (void) setUpAssetWriter
{
    NSError* error = nil;

    NSDictionary *compressionAudioSettings = @{
        AVFormatIDKey         : kRCOutputAudioStreamFormat,
        AVSampleRateKey       : @(self.mDeviceASBD->mSampleRate),
        AVNumberOfChannelsKey : @(self.mDeviceASBD->mChannelsPerFrame)
    };
    
    AVAssetWriterInput *assetWriterInput = [AVAssetWriterInput
    	assetWriterInputWithMediaType:AVMediaTypeAudio
        outputSettings:compressionAudioSettings
    ];
    assetWriterInput.expectsMediaDataInRealTime = YES;
	
    self.mAssetWriter = [AVAssetWriter
    	assetWriterWithURL:self.currentRecord.fileURL
        fileType:kRCAudioFileFormat
        error:&error
    ];
    
    NSString * msg = [NSString
        stringWithFormat:@"Failed to initialize AssetWriter: %@",
        error.description
    ];
    RCFail(error == nil, msg);
    
    [self.mAssetWriter addInput:assetWriterInput];


    [self.mAssetWriter addObserver:self forKeyPath:@"status"
       options:NSKeyValueObservingOptionNew context:nil
    ];
}

- (void) observeValueForKeyPath:(NSString *)keyPath
    ofObject:(id)object
    change:(NSDictionary<NSString *, id> *)change
    context:(void *)context
{
    if (self.mAssetWriter.status == AVAssetWriterStatusFailed)
    {
        [[ErrorHandler sharedInstance] handleIfNeeded:self.mAssetWriter.error];
        [self resetRecorder];
    }
}

- (void)resetRecorder
{
    self.initialized = NO;
    self.running = NO;
    self.mIsPaused = NO;
    self.mCaptureDevice = nil;
    self.mCaptureInput = nil;
    
    free(self.mDeviceASBD);
    self.mDeviceASBD = NULL;
    
    self.mCaptureOutput = nil;
    self.mCaptureSession = nil;
    [self.mAssetWriter removeObserver:self forKeyPath:@"status"];
    self.mAssetWriter = nil;
    self.mCurrentBufferTime = CMTimeMake(0, 1);

    self.currentRecord = nil;
}

/*============================================================================*/

- (void) interrupt
{
    if (self.running)
        [self.recordedAudioStreamObserver recordingWasInterrupted];
}

- (void) startRecording: (Record*)record
{
    @try
    {
        if (!self.initialized)
            [self setUpRecorder:record];
        [self makeAudioSessionActive:YES];

        [self setUpRecorder:record];
        self.mIsPaused = NO;
        [self.mCaptureSession startRunning];
        self.running = YES;
        NSLog(@"Start recording");
    }
    @catch (NSException * e)
    {
        [[ErrorHandler sharedInstance] handle:e.name];
        [self resetRecorder];
    }
}

- (void) pauseRecording
{
    if (! self.initialized)
        return;

    self.mIsPaused = YES;
    self.running = NO;
    NSLog(@"Pause recording");
}

- (void) stopRecording
{
    if (! self.initialized)
        return;

    @try
    {
        // Ensure that there won't be any buffer after session stop
        dispatch_sync(self.mRecorderQueue, ^{
            [self.mCaptureSession stopRunning];
        });

        [self.mAssetWriter.inputs[0] markAsFinished];
        [self.mAssetWriter finishWritingWithCompletionHandler:^{}];
        [self makeAudioSessionActive:NO];
    }
    @catch (NSException * e)
    {
        [[ErrorHandler sharedInstance] handle:e.name];
    }
    @finally
    {
        [self resetRecorder];
        NSLog(@"Stop recording");
    }
}

/*============================================================================*/

- (void) captureOutput:(AVCaptureOutput *)captureOutput
		didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       	fromConnection:(AVCaptureConnection *)connection
{
    @try
    {
        [self processBuffer:sampleBuffer];
    }
    @catch (NSException *e)
    {
        [self resetRecorder];
    }
}


- (void) processBuffer:(CMSampleBufferRef)sampleBuffer
{
    if (self.mIsPaused)
        return;
    [self setUpAudioStreamFormatDependentPartIfNeeded:sampleBuffer];

    CMSampleBufferRef sampleBufferWithCorrectTime =
        [self createSampleBufferWithFixedTiming:sampleBuffer];
    [self notifyAudioStreamObserver:sampleBufferWithCorrectTime];

    [self notifyRecordObserverAboutDurationChange: self.mCurrentBufferTime];

    self.mCurrentBufferTime = CMTimeAdd(
        self.mCurrentBufferTime
        ,	CMSampleBufferGetDuration(sampleBuffer)
    );
    BOOL success =
        [self.mAssetWriter.inputs[0] appendSampleBuffer:sampleBufferWithCorrectTime];
    RCFail(success, @"Can't append sample buffer");

    CFRelease(sampleBufferWithCorrectTime);
}

- (CMSampleBufferRef) createSampleBufferWithFixedTiming:
	(CMSampleBufferRef)sampleBuffer
{
    CMItemCount samplesCount = CMSampleBufferGetNumSamples(sampleBuffer);
    CMSampleTimingInfo newTimingInfo[samplesCount];
    
    CMItemCount sampleTimingEntriesCount;
    CMSampleBufferGetSampleTimingInfoArray(
    	sampleBuffer, samplesCount,
        newTimingInfo, &sampleTimingEntriesCount
    );
    
    for (CMItemCount i = 0; i < sampleTimingEntriesCount; ++i)
	{
    	newTimingInfo[i].presentationTimeStamp = self.mCurrentBufferTime;
    	newTimingInfo[i].decodeTimeStamp = newTimingInfo[i].presentationTimeStamp;
    }
    
    CMSampleBufferRef sampleBufferWithCorrectTime = nil;
    CMSampleBufferCreateCopyWithNewTiming(
    	kCFAllocatorDefault, sampleBuffer,
        sampleTimingEntriesCount, newTimingInfo,
        &sampleBufferWithCorrectTime
    );
    
	return sampleBufferWithCorrectTime;
}

- (void) notifyRecordObserverAboutDurationChange: (CMTime)bufferTime
{
    [self.recorderObserver recordedDurationDidChange:
        (NSInteger) (bufferTime.value / bufferTime.timescale)
    ];
}

- (void) notifyAudioStreamObserver: (CMSampleBufferRef)sampleBuffer
{
	CMItemCount samplesCount = CMSampleBufferGetNumSamples(sampleBuffer);
    CMBlockBufferRef samplesData = CMSampleBufferGetDataBuffer(sampleBuffer);
    size_t frameOffset = 0;
    for (CMItemCount i = 0; i < samplesCount; i+=1)
    {
        size_t sampleSize = CMSampleBufferGetSampleSize(sampleBuffer, i);
        char tempData[sampleSize];
        char * sampleDataPtr = NULL;
        
        CMSampleTimingInfo timingInfo;
        OSStatus res = CMBlockBufferAccessDataBytes(
        	samplesData, frameOffset,
            sampleSize, tempData, &sampleDataPtr
        );
        RCFail(kCMBlockBufferNoErr == res, @"Failed to get sample data");
        
        res = CMSampleBufferGetSampleTimingInfo ( sampleBuffer, i, &timingInfo );
        RCFail(kCMBlockBufferNoErr == res, @"Failed to get timing info");
        NSTimeInterval time = (double)timingInfo.presentationTimeStamp.value /
        	timingInfo.presentationTimeStamp.timescale;

        assert(timingInfo.presentationTimeStamp.timescale > self.mStreamObserverNotifyingRate);
        NSInteger lowerFrequency = timingInfo.presentationTimeStamp.timescale / self.mStreamObserverNotifyingRate;
        if (timingInfo.presentationTimeStamp.value % lowerFrequency)
            continue;
        double amplitude = [self extractAmplitudeValue:sampleDataPtr];
        [self.recordedAudioStreamObserver handleAmplitude:amplitude forTime:time];
     	frameOffset += sampleSize;
    }
}

- (double) extractAmplitudeValue: (char*)pSample
{
    static const int bitsInByte = 8;
    if (! (self.mDeviceASBD->mFormatFlags & kAudioFormatFlagIsFloat) )
    {
        if (self.mDeviceASBD->mFormatFlags & kAudioFormatFlagIsSignedInteger)
        {
            switch (self.mDeviceASBD->mBitsPerChannel / bitsInByte) {
                case sizeof(int16_t):
                    return *((int16_t *) pSample); // make checking of this option first
                case sizeof(int8_t):
                    return *((int8_t *) pSample);
                case sizeof(int32_t):
                    return *((int32_t *) pSample);
            }
        }
        else
        {
            switch (self.mDeviceASBD->mBitsPerChannel / bitsInByte) {
                case sizeof(uint8_t):
                    return *((uint8_t *) pSample);
                case sizeof(uint16_t):
                    return *((uint16_t *) pSample);
                case sizeof(uint32_t):
                    return *((uint32_t *) pSample);
            }
        }
    }
    else
    {
        switch (self.mDeviceASBD->mBitsPerChannel / bitsInByte)
        {
            case sizeof(float):
                return *((float*)pSample);
            case sizeof(double):
                return *((double*)pSample);
        }
    }
    RCFail(NO, @"Unsupported mic device: output stream depth");
    return 0.0;
}

@end
